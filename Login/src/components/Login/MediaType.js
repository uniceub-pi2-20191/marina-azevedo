import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Button } from 'react-native';

class MediaType extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style = {styles.container}>
        <TouchableOpacity style={[styles.buttonContainer, styles.loginButton]}
          onPress={() => this.props.navigation.navigate('Tela4')}>
          <Text style={styles.loginText}>Anime</Text>
        </TouchableOpacity>
        <TouchableOpacity style={[styles.buttonContainer, styles.loginButton]}
          onPress={() => this.props.navigation.navigate('Tela5')}>
          <Text style={styles.loginText}>Series</Text>
        </TouchableOpacity>
        <TouchableOpacity style={[styles.buttonContainer, styles.loginButton]}
          onPress={() => this.props.navigation.navigate('Tela6')}>
          <Text style={styles.loginText}>Movies</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container:{  
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#5EFF46',
  },
  buttonContainer:{
    height:45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom:20,
    width:250,
    borderRadius:30,
  },
  loginButton: {
    backgroundColor: '#ffff',
  },
});

export default MediaType;