import React, { Component } from 'react';
import { View, Text, TextInput, Button, Alert, Image, TouchableOpacity, StyleSheet } from 'react-native';
import firebase from 'react-native-firebase';

export default class Login extends Component {
  constructor(props) {
super(props);

this.state = {
Login: '',
Senha: '',
};

};

validarLogin = async () => {

const {Login, Senha } = this.state;
if ((Login == '' && Senha == "") || (Login == '' || Senha == "")) {
Alert.alert('Insira usuário e/ou senha.');

}
else {
try {
await firebase.auth().signInWithEmailAndPassword(Login,Senha);
this.props.navigation.navigate('Tela3')
}
            catch(err){
                Alert.alert('Usuario ou senha invalidos.');  
            }
}
}

  render() {
    return (
      <View style = {styles.container}>
        <View style = {styles.container}>
            <TextInput style = {styles.input}
                keyboardType="email-address"
                placeholder='E-mail'
                autoCapitalize="none"
                onChangeText={Login => this.setState({ Login })}
            />


            <TextInput style = {styles.input}
                secureTextEntry = {true}
                placeholder='Senha'
                autoCapitalize="none"
                onChangeText={Senha => this.setState({ Senha })}
            />
               
            <Button style={styles.buttonContainer}
                title = "Login"
                onPress={this.validarLogin}>
                onPress={() => this.props.navigation.navigate('Tela3')}>

            </Button>
        </View>

        <TouchableOpacity style = {styles.buttonContainer}
            onPress={() => this.props.navigation.navigate('Tela2')} >
<Text> Criar conta gratuita </Text>
</TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: '#5EFF46',
    },
    buttonContainer:{
        height:45,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 20,
        borderRadius: 30,
    },
    input: {
        marginLeft:16,
        borderBottomColor: 'rgba(255,255,255,0.3)',
        flex:0,
    },
});