import React, {Component} from 'react';
import {View, StyleSheet, Image, Text, KeyboardAvoidingView} from 'react-native';
import LoginForm from './LoginForm';

export default class Login extends Component {
    render(){
        return (
            <KeyboardAvoidingView behavior="padding" style={styles.container}>
                <View style={styles.logoContainer}>
                    <Image
                    style={style.logo}
                    source={require('../images/tartaruga.jpg')}
                    />
                    <Text style={style.title}>An app made by Marina and Vitória</Text>
                </View>
                <View style={style.formContainer}>
                    <LoginForm/>
                </View>
            </KeyboardAvoidingView>
            );
    }
}
const styles = StyleSheet.create({
    container:{
        backgroundColor:'#E8AEB4',
        flex:1,
    },
    logoContainer:{
        alignItems:'center',
        flexGlow:1,
        justifyContent:'center'
    },
    logo:{
        width:100,
        weight:100
    },
    title:{
        color:'#000000',
        marginTop:10,
        width:160,
        textAlign:'center',
        opacity:0.8
    }
});
